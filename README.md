## run a command to create django project:
 docker-compose run app sh -c "django-admin.py startproject app ."

## other commands used:
 - docker-compose build
 - docker build .

 ## create app
    docker-compose run --rm app sh -c 'python manage.py startapp recipe' 

## running tests
    docker-compose run app sh -c 'python manage.py test'

## creating a migration
     docker-compose run app sh -c 'python manage.py makemigrations core'

## create superuser 
    docker-compose run app sh -c "python manage.py createsuperuser"

## make migration
     docker-compose run --rm  app sh -c "python manage.py makemigrations"

## steps to create a new endpoint
    1. unit test for model
    2. create the model
    3. create migration
    4. register to admin (admin.py in core) 
        admin.site.register(models.Recipe)
    5. run tests again
    6. add tests for api (listings)
    7. create serializer
    8. create viewset
    9. register router

### Create a recipe detail endpoint (example)
    1. add tests
    2. add serializer
    3. modify viewset to return apropriate serializer


### Create a recipe endpoint (example)
    1. add tests
    2. modify the views.py (add perform_create)
    3. run tests
    4. restart the docker-compose
    5. test in the browser

## Add upload image endpoint
    1. add pillow requirements
    2. modify recipe model
        - create tests for the function to name files
        - create a function to name files
        - add image field
        - create migration
        - run tests
    3. add tests for uploading image to recipe
    4. add feature to upload image
        - create serializer
        - modify viewset

## Add filtering
    1. add tests 
    2. implement feature (modify get_queryset to filter)
    3. add tests for filtering tags and ingredients



