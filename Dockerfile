# image you are going to build your docker on
FROM python:3.7-alpine

# maintainer line:
LABEL Merima Begovic

ENV PYTHONUNBUFFERED 1

# dependencies
COPY ./requirements.txt /requirements.txt
RUN apk add --update --no-cache postgresql-client jpeg-dev
RUN apk add --update --no-cache --virtual .tmp-build-deps \
    gcc libc-dev linux-headers postgresql-dev musl-dev zlib zlib-dev
RUN pip install -r /requirements.txt

# delete temporary requrments
RUN apk del .tmp-build-deps

RUN mkdir /app
# default dir
WORKDIR /app
COPY ./app app

# add volume for storing media files (recipe images)
RUN mkdir -p /vol/web/media
# static files
RUN mkdir -p /vol/web/static

# user that is going to be used to run processes
# security purposes (to not run as root)
RUN adduser -D user

# change the ownership and permissions of the files to the created user
RUN chown -R user:user /vol/
RUN chmod -R 755 /vol/web

USER user
